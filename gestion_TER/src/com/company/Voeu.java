package com.company;

public class Voeu {
    private Sujet sujet;
    private Groupe groupe;
    private int priorité;

    public Voeu(Sujet sujet, Groupe groupe, int priorité) {
        this.sujet = sujet;
        this.groupe = groupe;
        this.priorité = priorité;
    }

    public Sujet getSujet() {
        return sujet;
    }

    public Groupe getGroupe() {
        return groupe;
    }

    public int getPriorité() {
        return priorité;
    }
}
