package fds.poste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestLettre {
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	Lettre lettre1 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, false);
	Lettre lettre2 = new Lettre("Le pere Noel",
			"famille Kouk, igloo 2, banquise nord",
			"5854", 18, 0.00018f, Recommandation.deux, true);
	Colis colis1 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200); 
	SacPostal sac1 = new SacPostal();
	
	
	@Test
    public void testColisToString()
    {
        assertEquals( colis1.toString(), "Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0");
    }
    @Test
    public void testColisAffranchis()
    {
    	assertEquals(colis1.tarifAffranchissement(), 3.5f, tolerancePrix);
    }
    
    @Test
    public void testColisRemboursement()
    {
    	assertEquals(colis1.tarifRemboursement(), 100.0f, tolerancePrix);
    }
    
    @Test
    public void testLettreToString()
    {
        assertEquals( lettre1.toString(), "Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire");
        assertEquals( lettre2.toString(), "Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence");
    }
    @Test
    public void testLettreAffranchis()
    {
    	assertEquals(lettre1.tarifRemboursement(), 1.5f, tolerancePrix);
    	assertEquals(lettre2.tarifAffranchissement(), 2.3f, tolerancePrix);
    }
    
    @Test
    public void testLettretRemboursement()
    {
        assertEquals(lettre2.tarifRemboursement(), 15.0f, tolerancePrix);
        assertEquals(lettre1.tarifRemboursement(), 1.5f, tolerancePrix);

    }
    
    @Test 
    public void testSacPostal() 
    {
    	sac1.ajoute(lettre1);
    	sac1.ajoute(lettre2);
    	sac1.ajoute(colis1);
    	assertEquals(sac1.valeurRemboursement(), 116.5f, tolerancePrix);
    	assertEquals(sac1.getVolume(), 0.025359999558422715f, toleranceVolume);
        
		SacPostal sac2 = sac1.extraireV1("7877");

		assertEquals(sac2.getVolume(), 0.02517999955569394f, toleranceVolume);

    }
    
	
    
}
